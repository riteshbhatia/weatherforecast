# Forecast Sample


This is a sample project for showing the coding practices followed usually while developing small scale apps. In this application, I have used the following libraries and practices:

- Retrofit with Coroutines support for API calling
- Koin for dependency Injection
- Live data as observable data class
- MVVM pattern for separation of concerns
- Kotlin as programming language


Work done:

- The App is able to fetch the data from API for a specific city for next 5 days
- The app is responding to various app states such as loading, error, and success

To be done:

- Need to work more on orientation changes
- Need to manage no internet connection 
- Improve the UI design
- Write Unit and UI test cases


