package com.practice.weather.coding.application

import android.app.Application
import com.practice.weather.coding.fragment.fragmentModule
import com.practice.weather.coding.viewmodel.viewModelModule
import com.practice.weather.coding.repository.forecastModule
import com.practice.weather.coding.networking.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class WeatherApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@WeatherApp)
            modules(listOf(fragmentModule,
                viewModelModule, networkModule,
                forecastModule
            ))
        }
    }
}