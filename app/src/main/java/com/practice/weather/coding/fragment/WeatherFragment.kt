package com.practice.weather.coding.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.OnClickListener
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.practice.weather.coding.R
import com.practice.weather.coding.R.string
import com.practice.weather.coding.model.Forecast
import com.practice.weather.coding.networking.Resource
import com.practice.weather.coding.networking.Status
import com.practice.weather.coding.viewmodel.WeatherViewModel
import kotlinx.android.synthetic.main.fragment_weather.forecast_btn
import kotlinx.android.synthetic.main.fragment_weather.forecast_status_tv
import kotlinx.android.synthetic.main.fragment_weather.location_et
import kotlinx.android.synthetic.main.fragment_weather.progressBar
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.dsl.module

val fragmentModule = module {
  factory {
    WeatherFragment()
  }
}

class WeatherFragment : Fragment(), OnClickListener {
  private val weatherViewModel: WeatherViewModel by viewModel()

  private val observer = Observer<Resource<Forecast>> {
    when (it.status) {
      Status.SUCCESS -> displayForecast(it.data!!)
      Status.ERROR -> showError(it.message!!)
      Status.LOADING -> showLoading()
    }
  }

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? = inflater.inflate(R.layout.fragment_weather, container, false)

  override fun onViewCreated(
    view: View,
    savedInstanceState: Bundle?
  ) {
    super.onViewCreated(view, savedInstanceState)
    retainInstance = true
    weatherViewModel.weather.observe(this, observer)
    forecast_btn.setOnClickListener(this)
  }

  private fun showLoading() {
    forecast_status_tv.text = getString(string.loading_text)
    progressBar.visibility = VISIBLE
  }

  private fun showError(message: String) {
    progressBar.visibility = GONE
    forecast_status_tv.text = getString(string.error_msg, message)
  }

  private fun displayForecast(forecast: Forecast) {
    progressBar.visibility = GONE
    forecast_status_tv.visibility = GONE

    val ft = fragmentManager!!.beginTransaction()
    val prev = fragmentManager!!.findFragmentByTag(TAG_FORECAST_LIST_FRAGMENT)
    if (prev != null) {
      ft.remove(prev)
    }

    ft.replace(R.id.root, ForecastListFragment.newInstance(forecast), TAG_FORECAST_LIST_FRAGMENT)
        .addToBackStack(
            TAG_FORECAST_LIST_FRAGMENT
        )
        .commit()
  }

  override fun onClick(v: View?) {
    when (v) {
      forecast_btn -> weatherViewModel.getWeather(location_et.text.toString())
    }
  }

  companion object {
    private const val TAG_FORECAST_LIST_FRAGMENT = "forecast"

  }
}