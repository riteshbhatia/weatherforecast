package com.practice.weather.coding.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.practice.weather.coding.R
import com.practice.weather.coding.fragment.WeatherFragment
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {
    private val weatherFragment: WeatherFragment by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction().replace(R.id.root, weatherFragment, TAG_WEATHER_FRAGMENT).commit()
    }

    companion object{
        private const val TAG_WEATHER_FRAGMENT = "weather"
    }
}
