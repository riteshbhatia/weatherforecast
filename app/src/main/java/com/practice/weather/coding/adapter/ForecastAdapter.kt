package com.practice.weather.coding.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.practice.weather.coding.R
import com.practice.weather.coding.model.WeatherDetail
import kotlinx.android.synthetic.main.item_forecast.view.day_tv
import kotlinx.android.synthetic.main.item_forecast.view.humidity
import kotlinx.android.synthetic.main.item_forecast.view.max_temp_tv
import kotlinx.android.synthetic.main.item_forecast.view.min_temp_tv
import kotlinx.android.synthetic.main.item_forecast.view.temperature_tv

class ForecastAdapter(weatherDetails: List<WeatherDetail>) : RecyclerView.Adapter<ForecastAdapter.ForecastViewHolder>() {

  val weatherDetails: List<WeatherDetail> by lazy { weatherDetails }

  override fun onCreateViewHolder(
    parent: ViewGroup,
    viewType: Int
  ): ForecastViewHolder {
    val itemView = LayoutInflater.from(parent.context)
        .inflate(R.layout.item_forecast, parent, false)
    return ForecastViewHolder(itemView)
  }

  override fun getItemCount() = weatherDetails.size

  override fun onBindViewHolder(
    holder: ForecastViewHolder,
    position: Int
  ) {
    holder.bind(position)
  }

  inner class ForecastViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(
      position: Int
    ) = with(itemView) {
      val dayForecast = weatherDetails[position]
      day_tv.text = dayForecast.dt_txt
      temperature_tv.text = resources.getString(R.string.current_temp,dayForecast.main.temp.toString())
      max_temp_tv.text = resources.getString(R.string.max_temp,dayForecast.main.temp_max.toString())
      min_temp_tv.text = resources.getString(R.string.min_temp,dayForecast.main.temp_min.toString())
      humidity.text = resources.getString(R.string.humidity,dayForecast.main.humidity.toString())
    }
  }
}


