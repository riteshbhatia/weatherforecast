package com.practice.weather.coding.networking

import com.practice.weather.coding.model.Forecast
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {

    @GET("forecast")
    suspend fun getFiveDaysForecast(@Query("q")location: String): Forecast
}