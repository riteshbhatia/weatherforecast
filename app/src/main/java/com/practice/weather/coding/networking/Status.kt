package com.practice.weather.coding.networking

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}