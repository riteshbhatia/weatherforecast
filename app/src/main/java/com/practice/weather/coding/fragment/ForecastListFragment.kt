package com.practice.weather.coding.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.practice.weather.coding.R
import com.practice.weather.coding.adapter.ForecastAdapter
import com.practice.weather.coding.model.Forecast
import kotlinx.android.synthetic.main.fragment_forecast_list.city_tv
import kotlinx.android.synthetic.main.fragment_forecast_list.forecast_list

class ForecastListFragment() : Fragment() {


  private lateinit var forecastData: Forecast
  private lateinit var forecastAdapter: ForecastAdapter


  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View?  = inflater.inflate(R.layout.fragment_forecast_list, container, false)

  override fun onViewCreated(
    view: View,
    savedInstanceState: Bundle?
  ) {
    super.onViewCreated(view, savedInstanceState)
    retainInstance = true
    forecastData = arguments!!.getParcelable(ARG_FORECAST)!!
    forecastAdapter = ForecastAdapter(forecastData.list)
    city_tv.text = forecastData.city.name
    forecast_list.layoutManager = LinearLayoutManager(context)
    forecast_list.adapter = forecastAdapter
    forecastAdapter.notifyDataSetChanged()
  }

  companion object{
    private const val ARG_FORECAST = "forecastData"

    fun newInstance(forecast: Forecast): ForecastListFragment {
      val forecastListFragment = ForecastListFragment()
      val bundle = Bundle().apply {
        putParcelable(ARG_FORECAST, forecast)
      }
      forecastListFragment.arguments = bundle
      return  forecastListFragment
    }

  }

}