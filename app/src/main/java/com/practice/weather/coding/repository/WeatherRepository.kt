package com.practice.weather.coding.repository

import com.practice.weather.coding.model.Forecast
import com.practice.weather.coding.networking.Resource
import com.practice.weather.coding.networking.ResponseHandler
import com.practice.weather.coding.networking.WeatherApi
import org.koin.dsl.module

val forecastModule = module {
    factory { WeatherRepository(get(), get()) }
}

open class WeatherRepository(
    private val weatherApi: WeatherApi,
    private val responseHandler: ResponseHandler
) {

    suspend fun getForecast(location: String): Resource<Forecast> {
        return try {
            val response = weatherApi.getFiveDaysForecast(location)
            return responseHandler.handleSuccess(response)
        } catch (e: Exception) {
            responseHandler.handleException(e)
        }
    }
}