package com.practice.weather.coding.viewmodel

import androidx.lifecycle.*
import com.practice.weather.coding.repository.WeatherRepository
import com.practice.weather.coding.networking.Resource
import kotlinx.coroutines.Dispatchers
import org.koin.dsl.module

val viewModelModule = module {
    factory { WeatherViewModel(get()) }
}

class WeatherViewModel(
    private val weatherRepo: WeatherRepository
) : ViewModel() {

    private val location = MutableLiveData<String>()

    fun getWeather(input: String) {
        location.value = input
    }

    var weather = location.switchMap { location ->
        liveData(Dispatchers.IO) {
            emit(Resource.loading(null))
            emit(weatherRepo.getForecast(location))
        }
    }
}
