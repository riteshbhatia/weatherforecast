package com.practice.weather.coding.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Forecast( val city: City,
    val cnt: Int,
    val cod: String,
    val list: List<WeatherDetail>,
    val message: Double): Parcelable